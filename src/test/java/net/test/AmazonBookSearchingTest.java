package net.test;

import com.test.actions.Actions;
import com.test.base.BaseTest;
import com.test.util.Constants;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Set;

public class AmazonBookSearchingTest extends BaseTest {
    private String query = "Java";
    private String bookUrl = "https://www.amazon.com/Head-First-Java-Kathy-Sierra/dp/0596009208/ref=sr_1_3";

    @Test
    public void searchBook(){
        // Making query on Amazon main page
        driver.get(Constants.BASE_URL);
        Actions.amazonActions().searchGoods(query);
        Set<String> titles= Actions.amazonActions().getBooks().keySet();

        driver.get(bookUrl);
        String title = Actions.amazonActions().getBookTitleByPage();
        Assert.assertTrue(titles.contains(title));
    }

}
