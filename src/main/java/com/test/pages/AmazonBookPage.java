package com.test.pages;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;

public class AmazonBookPage extends BasePage {
    Locator titleElement = new XPath("//span[@id='productTitle']");

    public String getTitle() {
        waitForElementVisibility(titleElement);
        return getElement(titleElement).getText();
    }
}
