package com.test.pages;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;

public class AmazonMainPage extends BasePage {
    Locator searchField = new XPath("//input[@id='twotabsearchtextbox']");
    Locator submitButton = new XPath("//input[@value='Go']");

    public void insertIntoSearchField(String query) {
        waitForElementVisibility(searchField);
        type(String.format("Query %s is passed to search field", query), query, searchField);
    }

    public void submitSearch() {
        waitForElementToBeClickable(submitButton);
        click("Query is submitted", submitButton);
    }
}
