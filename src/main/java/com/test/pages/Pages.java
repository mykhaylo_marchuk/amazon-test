package com.test.pages;

public class Pages {
    private static LoginPage loginPage;
    private static AmazonMainPage amazonMainPage;
    private static AmazonResultsPage amazonResultsPage;
    private static AmazonBookPage amazonBookPage;

    public static LoginPage loginPage() {
        if (loginPage == null){
            loginPage = new LoginPage();
        }
        return loginPage;
    }

    public static AmazonMainPage amazonMainPage() {
        if(amazonMainPage == null) {
            amazonMainPage = new AmazonMainPage();
        }
        return amazonMainPage;
    }

    public static AmazonResultsPage amazonResultsPage() {
        if(amazonResultsPage == null) {
            amazonResultsPage = new AmazonResultsPage();
        }
        return amazonResultsPage;
    }

    public static AmazonBookPage amazonBookPage() {
        if(amazonBookPage == null) {
            amazonBookPage = new AmazonBookPage();
        }
        return amazonBookPage;
    }
}
