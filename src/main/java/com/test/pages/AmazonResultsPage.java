package com.test.pages;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

public class AmazonResultsPage extends BasePage {
    Locator resultsContainer = new XPath("//div[contains(@class, 's-search-results')]");

    public List<BookContainer> getSearchResults() {
        waitForElementVisibility(resultsContainer);
        return getElement(resultsContainer).findElements(By.xpath("./*"))
                .stream().map(BookContainer::new).collect(Collectors.toList());
    }
}
