package com.test.pages;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class BookContainer extends BasePage {
    Locator titleElement = new XPath(".//h2");
    Locator authorElement = new XPath(".//a[contains(@class, 'a-size-base a-link-normal')]");

    String title;
    String author;

    public BookContainer(WebElement bookElement) {
        this.title = bookElement.findElement(titleElement.get()).getText();
        this.author = bookElement.findElement(authorElement.get()).getText();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
