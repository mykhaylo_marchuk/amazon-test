package com.test.actions;

import com.test.base.BaseActions;
import com.test.pages.BookContainer;
import com.test.pages.Pages;

import java.util.HashMap;
import java.util.Map;

public class AmazonActions extends BaseActions {
    public void searchGoods(String query) {
        Pages.amazonMainPage().insertIntoSearchField(query);
        Pages.amazonMainPage().submitSearch();
    }

    public String getBookTitleByPage() {
        return Pages.amazonBookPage().getTitle();
    }

    public Map<String, String> getBooks() {
        return new HashMap<String, String>() {{
            for(BookContainer book : Pages.amazonResultsPage().getSearchResults()) {
                System.out.println(book.getTitle());
                put(book.getTitle(), book.getAuthor());
            }
        }};
    }
}
