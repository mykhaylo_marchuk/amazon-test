package com.test.actions;

public class Actions {

    private static MainActions mainActions;
    private static LoginActions loginActions;
    private static AmazonActions amazonActions;

    public static MainActions mainActions() {
        if (mainActions == null) {
            mainActions = new MainActions();
        }
        return mainActions;
    }

    public static LoginActions loginActions() {
        if (loginActions == null) {
            loginActions = new LoginActions();
        }
        return loginActions;
    }

    public static AmazonActions amazonActions() {
        if (amazonActions == null) {
            amazonActions = new AmazonActions();
        }
        return amazonActions;
    }
}

